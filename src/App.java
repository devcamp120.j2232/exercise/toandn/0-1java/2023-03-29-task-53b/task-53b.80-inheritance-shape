public class App {
    public static void main(String[] args) throws Exception {
        Shape shape1 = new Shape();
        Shape shape2 = new Shape("green", false);
        System.out.println(shape1.toString());
        System.out.println(shape2.toString());

        Circle circle1 = new Circle();
        Circle circle2 = new Circle(2.0);
        Circle circle3 = new Circle("green", true, 3.0);
        System.out.println(circle1.toString());
        System.out.println(circle2.toString());
        System.out.println(circle3.toString());

        Rectangel rectangel1 = new Rectangel();
        Rectangel rectangel2 = new Rectangel(2.5, 1.5);
        Rectangel rectangel3 = new Rectangel("green", true, 2.0, 1.5);
        System.out.println(rectangel1.toString());
        System.out.println(rectangel2.toString());
        System.out.println(rectangel3.toString());

        Square square1 = new Square();
        Square square2 = new Square(1.5);
        Square square3 = new Square("green", true, 2.0);
        System.out.println(square1.toString());
        System.out.println(square2.toString());
        System.out.println(square3.toString());

        System.out.println("Hinh tron");
        System.out.println("Dien Tich: " + circle1.getArea() + ", Chu Vi: " + circle1.getPerimeter());
        System.out.println("Dien Tich: " + circle2.getArea() + ", Chu Vi: " + circle2.getPerimeter());
        System.out.println("Dien Tich: " + circle3.getArea() + ", Chu Vi: " + circle3.getPerimeter());

        System.out.println("Hinh Chu Nhat");
        System.out.println("Dien Tich: " + rectangel1.getArea() + ", Chu Vi: " + rectangel1.getPerimeter());
        System.out.println("Dien Tich: " + rectangel2.getArea() + ", Chu Vi: " + rectangel2.getPerimeter());
        System.out.println("Dien Tich: " + rectangel3.getArea() + ", Chu Vi: " + rectangel3.getPerimeter());

        System.out.println("Hinh Vuong");
        System.out.println("Dien Tich: " + square1.getArea() + ", Chu Vi: " + square1.getPerimeter());
        System.out.println("Dien Tich: " + square2.getArea() + ", Chu Vi: " + square2.getPerimeter());
        System.out.println("Dien Tich: " + square3.getArea() + ", Chu Vi: " + square3.getPerimeter());

    }
}
